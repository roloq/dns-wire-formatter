# dns-wire-formatter

## Description
This program lets you convert a list of DNS Domain names in wire format to a presentation format.
This project is written in C++ for being executed on a Linux system.

## Getting Started

In order to get a copy of the source code you must download it from the following git repository: https://bitbucket.org/roloq/dns-wire-formatter.git
```
git clone https://bitbucket.org/roloq/dns-wire-formatter.git
```

### Prerequisites

To compile the source code you will need to install the following tools:

*  gcc and g++ - The C and C++ compilers
```
sudo dnf install gcc gcc-c++
```
*  Git - The version control system
```
sudo dnf install git-all
```
*  CMake - Tool designed to build, test and package software
```
sudo dnf install cmake
```

### Compiling the code

These are the steps to compile the code and unit tests.
*  Go to the directory where you downloaded the source code
*  Execute the following commands:
```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```
**Note:** If you want to add debug symbols then run this command instead of "cmake -DCMAKE_BUILD_TYPE=Release .."
```
cmake -DCMAKE_BUILD_TYPE=Debug ..
```

### Running the executable

If you want to run the executable please follow these commands from the root directory of the source code:
```
cd build
./dns-wire-formatter < "/path/to/your/wire/data/file"
```
**Note:** 
*  The text "/path/to/your/wire/data/file" must be replaced with the path to the file that contains the wire data to be converted to a presentation format.
*  If the command was executed succesfully then you will see the wire data converted to a presentation data format.
*  If you don't see any data displayed then your wire data file contains invalid data. You can confirm it by executing the following command:
```
echo $?
```
If you see a value different to zero then something wrong happened during the conversion from wire data to presentation data.

### Running the tests
There are thirty unit tests that were written to test each method in the project.
*  If you want to run the test cases then please follow these steps from the root directory of the source code:
```
cd build/test/
./test-dns-wire-formatter
```
A message will be displayed and it will say that 30 tests passed.
**Note:** If you want to read the documentation of the unit tests, please read the Section 5.5.1 in the documentation.pdf file that is in the "docs" directory.

### Reading the documentation

If you want to read the documentation of the code then please follow this step from the root directory of the source code:
```
cd docs
```
Inside docs directory you will see an HTML directory and the "documentation.pdf" file.
*  If you want to see the PDF documentation, please open the "documentation.pdf" file with your preferred PDF reader.
*  If you want to see the HTML documentation, please follow these steps:
```
cd html
Open the index.html file with your favorite web browser.
```

### Installing the executable
In order to install the executable into /usr/local/bin/ directory please follow these steps from the root directory of the source code:
```
cd build
sudo make install
```
**Note:** The executable called "dns-wire-formatter" will be copied to /usr/local/bin/ directory.

After doing that, you can execute the program by following this step:
```
dns-wire-formatter < "/path/to/your/wire/data/file"
```
**Note:** The text "/path/to/your/wire/data/file" must be replaced with the path to the file that contains the wire data to be converted to a presentation format.

### Examples
If you want to see some examples of the usage of "dns-wire-formatter", please follow these steps from the root directory of your source code:

**Example 1:** This example is for testing a wire data file that contains a root label '0x00'.
```
./build/dns-wire-formatter < samples/inputRootLabel
```
The expected result is:
```
.
```

**Example 2:** This example is for testing a wire data file that contains two DNS domain names in wire format '\x03\x63\x6e\x6e\x03\x63\x6f\x6d\x00\x03\x62\x62\x63\x03\x63\x6f\x6d\x00'.
```
./build/dns-wire-formatter < samples/inputTwoDNSNames
```
The expected result is:
```
.bbc.com.
.cnn.com.
```

**Example 3:** This example is for testing a wire data file that contains three DNS domain names in wire format '\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d\x00\x03\x63\x6e\x6e\x03\x63\x6f\x6d\x00\x03\x62\x62\x63\x03\x63\x6f\x6d\x00'.
```
./build/dns-wire-formatter < samples/inputSeveralValidDNSname
```
The expected result is:
```
.bbc.com.
.cnn.com.
.espn.com.
```

**Example 4:** This example is for testing a wire data file that contains an invalid DNS domain name with more than 63 labels.
```
./build/dns-wire-formatter < samples/inputTooManyLabels
```
As the DNS domain name is invalid then nothing is going to be displayed on screen. 
If you want to confirm that "dns-wire-formatter" returned an error, you can check the exit code by executing the following command:
```
echo $?
```
It will return a value of 1.
```
1
```

**Example 5:** This example is for testing a wire data file that contains an invalid DNS domain name with more than 255 octets.
```
./build/dns-wire-formatter < samples/inputTooManyOctets
```
As the DNS domain name is invalid then nothing is going to be displayed on screen. 
If you want to confirm that "dns-wire-formatter" returned an error, you can check the exit code by executing the following command:
```
echo $?
```
It will return a value of 1.
```
1
```

**Example 6:** This example is for testing an empty wire data file.
```
./build/dns-wire-formatter < samples/inputEmpty
```
As the DNS domain name is empty then an empty line is going to be displayed in the command line.
```

```

### Generating the documentation of the code from scratch
If you want to generate again the documentation then you need to install and compile Doxygen.

**Note:** In case you have already installed Doxygen please go to the step number 5:

*  Step 1: Install flex
```
sudo dnf install -y flex
```

*  Step 2: Install bison
```
sudo dnf install -y bison
```

*  Step 3: Download Doxygen to your home directory and execute these steps:
```
cd ~
git clone https://github.com/doxygen/doxygen.git
```

*  Step 4: Compile it
```
cd ~
cd doxygen
mkdir build
cd build
cmake -G "Unix Makefiles" ..
make
sudo make install
```

*  Step 5: Generate the documentation of the code from the root directory of the project. Please run the following command:
```
doxygen config_file
```
**Note:** The documentation will be generated in docs/html directory. You can open "index.html" file by using a web browser in order to see the documentation of the project.

*  Step 6: You must follow these steps to generate the documentation.pdf file. Please execute them from the root directory of the source code:
```
sudo dnf install texlive-latex texlive-pdftex.x86_64 texlive-epstopdf.noarch texlive-dvips.noarch texlive-tabu.noarch texlive-xtab.noarch texlive-import.noarch texlive-appendix.noarch texlive-multirow.noarch texlive-hanging.noarch texlive-adjustbox.noarch texlive-stackengine.noarch texlive-ulem.noarch texlive-wasysym.noarch texlive-collection-fontsrecommended.noarch texlive-sectsty.noarch texlive-fancyhdr.noarch texlive-natbib.noarch texlive-tocloft.noarch texlive-newunicodechar.noarch texlive-caption.noarch texlive-etoc.noarch
cd docs/latex
make
cd ..
```
The document called "documentation.pdf" will be generated inside that directory 

## Author

* **Rolando Quesada**
