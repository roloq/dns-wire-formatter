#include <iostream>
#include "dnsWireFormatter.h"

using namespace std;

void displayUsageMessage()
{
	cout << "Invalid number of arguments\n"
			"Usage: ./dns-wire-formatter < /path/to/your/file" << endl;
}

int main(int argc, char * argv[])
{
	// Initializing wireData with argv[0] to avoid a warning related to unused argv[0]
	string wireData = argv[0];
	unsigned int numberOctets = 0;
	string presentationData = "";
	DnsWireFormatter dnsWireFormatter;
	RESULT result = RESULT::UNKNOWN;

	if (argc == 1)
	{
		wireData = "";
		std::istream* input = &std::cin; // input is stdin by default
		string line;
		while(*input)
		{
			if(std::getline(*input,line))
			{
				wireData = line;
			}
		}
		numberOctets = wireData.length();
		result = dnsWireFormatter.convertWireToPresentationData(wireData.c_str(), numberOctets, presentationData);
		if (RESULT::OK != result)
		{
			return 1;
		}

		// If the conversion worked correctly then we print the presentation data
		cout << presentationData << endl;
		return 0;
	}
	else
	{
		displayUsageMessage();
		return 1;
	}

}
