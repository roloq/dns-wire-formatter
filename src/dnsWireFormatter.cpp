#include <iostream>
#include <list>
#include "dnsWireFormatter.h"
#include <regex>

const unsigned char ROOT_OCTET_VALUE = '\x00';/**< This is the trailing zero that represents the root label */
const unsigned int MAX_NUMBER_LABELS = 63;/**< This is the maximum number of labels allowed in a DNS name */
const unsigned int MAX_NUMBER_OCTETS_PER_LABEL = 63;/**< This is the maximum number of octets allowed per label */
const unsigned int MAX_NUMBER_OCTETS = 255;/**< This is the maximum number of octets allowed in a DNS name */

DnsWireFormatter::DnsWireFormatter()
{
}

DnsWireFormatter::~DnsWireFormatter()
{
}

RESULT DnsWireFormatter::convertWireToPresentationData(const char *wireData,
		                                               const unsigned int &numberOctets,
													   string &presentationData)
{

	// This method is in charge of convert the wire data to presentation data
	RESULT result = RESULT::OK;
	presentationData = "";

	// If the wireData parameter is NULL then don't do anything
	if (nullptr == wireData)
	{
		return RESULT::INVALID_ARGUMENT;
	}

	// If there are octets to parse
	if (numberOctets > 0) {
		// If the wire data starts with a root label then we don't do anything
		if (wireData[0] != ROOT_OCTET_VALUE) {
			vector<string> dnsNamesVector;
			// First, we create a vector of dns names
			result = getDNSVectorFromWireData(wireData, numberOctets, dnsNamesVector);
			if (RESULT::OK != result) {
				return result;
			}

			// Then, we sort that vector of dns according to the Section 6.1 of RFC4034
			vector<string> dnsNamesVectorSorted;
			result = sortDNSNames(dnsNamesVector, dnsNamesVectorSorted);
			if (RESULT::OK != result) {
				return result;
			}

			presentationData = "";
			// Then, We format the data to be presented as requested
			// Finally, we return the presentation data as an OUT parameter
			result = convertToPresentationDataUtil(dnsNamesVectorSorted,
					presentationData);
			if (RESULT::OK != result) {
				presentationData = "";
				return result;
			}
		}
		else
		{
			// Note: Ask if the RFC specifies what to do in case the wire data contains 0x00 only as value
			// Assuming that in that case we must display a period sign
			presentationData = ".";
		}
	}

	return result;
}

// It creates a vector of DNS names based on the wire data
RESULT DnsWireFormatter::getDNSVectorFromWireData(const char *wireData,
		                                          const unsigned int &numberOctets,
												  vector<string> &dnsNames)
{
	RESULT result = RESULT::OK;
	unsigned int wireDataIndex = 0;
	unsigned int numOctetsRead = 0;
	string dnsName = "";

	// We make sure that the vector is empty
	dnsNames.clear();

	// If the wireData parameter is NULL then we don't do anything
	if (nullptr == wireData)
	{
		return RESULT::INVALID_ARGUMENT;
	}

	// If there is nothing to process then we return an empty vector of DNS domain names
	if (numberOctets == 0)
	{
		result = RESULT::OK;
		return result;
	}

	// if the wire data has a root label in the first octet
	if (wireData[0] == ROOT_OCTET_VALUE)
	{
		result = RESULT::OK;
		dnsNames.push_back(".");
		return result;
	}

	// While there is data to be parsed
	while((wireDataIndex < numberOctets) && (RESULT::OK == result))
	{
		dnsName = "";
		result = getFirstDNSNameFromWireData(wireData, numberOctets, wireDataIndex, dnsName, numOctetsRead);

		if(RESULT::OK == result)
		{
			dnsNames.push_back(dnsName);
			wireDataIndex += numOctetsRead;
		}
	}

	// We return an empty vector in case that something wrong happened
	if (RESULT::OK != result)
	{
		dnsNames.clear();
	}

	return result;
}

// It returns the first DNS name found in the wire data according to the fromPosition parameter
RESULT DnsWireFormatter::getFirstDNSNameFromWireData(const char *wireData,
													 const unsigned int &numberOctets,
													 const unsigned int &fromPosition,
		                                             string &dnsName,
													 unsigned int &numOctetsRead)
{
	RESULT result = RESULT::OK;
	unsigned int labelCounter = 0;
	unsigned int innerIndex = 0;
	unsigned int size = 0;
	unsigned int currentCursorPosition = fromPosition;

	dnsName = "";
	numOctetsRead = 0;
	labelCounter = 0;

	// If the wireData parameter is NULL then we don't do anything and return an empty DNS domain name
	if (nullptr == wireData)
	{
		return RESULT::INVALID_ARGUMENT;
	}

	// Assume that if wireData value is empty then we return an empty DNS domain name
	if (numberOctets == 0)
	{
		result = RESULT::OK;
		return result;
	}

	// If the wireData parameter starts with a root label
	if (wireData[0] == ROOT_OCTET_VALUE)
	{
		result = RESULT::OK;
		dnsName = ".";
		numOctetsRead = 1;

		return result;

	}

	// If the index is greater than the number of octets that the wire data has
	if (currentCursorPosition >= numberOctets)
	{
		result = RESULT::INVALID_ARGUMENT;
	}
	else
	{
		dnsName = ".";

		// We go through the wire data and we parse data until we reach the end of the wire data or we find a root label or something wrong happens
		while((numberOctets > currentCursorPosition) && (wireData[currentCursorPosition] != ROOT_OCTET_VALUE) && (RESULT::OK == result))
		{
			innerIndex = 0;
			size = wireData[currentCursorPosition];

			// Validate the number of labels must be less or equal to 63
			if (size > MAX_NUMBER_OCTETS_PER_LABEL)
			{
				result = RESULT::INVALID_NUMBER_OCTETS_PER_LABEL;
			}

			if (RESULT::OK == result)
			{
				// We parse each label and we encode non ASCII or dot characters
				while(innerIndex < size)
				{
					currentCursorPosition++;
					dnsName += getEncodedChar(wireData[currentCursorPosition]);
					innerIndex++;
				}

				labelCounter++;

				// If the DNS domain name has more than 63 labels
				if (labelCounter > MAX_NUMBER_LABELS)
				{
					result = RESULT::INVALID_NUMBER_LABELS;
				}

				// We move one position forward to continue with the following label
				currentCursorPosition++;

				// We append a dot character per each label found
				dnsName += ".";
			}

		}

		if (RESULT::OK == result)
		{
			// If the number of octets in the dns name is greater than 255
			if (dnsName.length() > MAX_NUMBER_OCTETS)
			{
				result = RESULT::INVALID_NUMBER_OCTETS;
			}
		}

	}

	// If something wrong happened then we return an error value and an empty DNS domain name
	if (RESULT::OK != result)
	{
		dnsName = "";
	}

	// We add one to the number of octets read in order to include the root octet
	numOctetsRead = currentCursorPosition - fromPosition + 1;
	return result;
}

// It sorts in a canonical order a list of DNS names according to the Section 6.1 of RFC4034
RESULT DnsWireFormatter::sortDNSNames(const vector<string> &dnsNamesVector,
		                              vector<string> &sortedDNSNames)
{

	// Make sure the vector is empty
	sortedDNSNames.clear();

	RESULT result = RESULT::OK;
	string name = "";
	vector<string> tempDNSnames;

	// if there are at least one DNS domain name to be sorted
	if (!dnsNamesVector.empty())
	{
		// First, let's iterate through the vector
		for (string dnsName: dnsNamesVector)
		{
			// Note: Assuming we are working with case insensitive ordering
			// If we need to convert the characters to lower case then we must sacrifice performance
			std::transform(dnsName.begin(), dnsName.end(), dnsName.begin(), ::tolower);

			// Then, reverse the dns name i.e .com.cnn. to .cnn.com.
			name = getReverseDNSName(dnsName);

			// insert the dns name into a new vector
			tempDNSnames.push_back(name);
		}

		// Then, sort the reversed dns names
		sort(tempDNSnames.begin(), tempDNSnames.end());

		// Then, we iterate through the reversed dns names vector
		for (string dnsName: tempDNSnames)
		{
			// Finally, reverse again the reverse dns name to get back the original dns name
			name = getReverseDNSName(dnsName);

			sortedDNSNames.push_back(name);
		}
	}

	return result;
}

// Utility method used to convert a vector of DNS names to a presentation data format
RESULT DnsWireFormatter::convertToPresentationDataUtil(const vector<string> &dnsNamesVector,
		                                               string &presentationData)
{
	RESULT result = RESULT::OK;

	presentationData = "";

	// Iterate through the vector to concatenate the DNS domain names
	for (unsigned int index = 0; index < dnsNamesVector.size(); index++)
	{
		presentationData += dnsNamesVector.at(index);

		if ((index + 1) < dnsNamesVector.size())
		{
			presentationData += "\n";
		}
	}

	return result;
}

// Utility method used to reverse a DNS name
// i.e. .cnn.com. -> .com.cnn.
string DnsWireFormatter::getReverseDNSName(const string &dnsName)
{
	string reverseDNSName = "";
	list<string> dnsNamesVector;

	if(!dnsName.empty())
	{
		regex re("\\.");

		// We create a list of labels separated by a dot character
		std::sregex_token_iterator it(dnsName.begin(), dnsName.end(), re, -1);
		std::sregex_token_iterator end;

		// We iterate the list of labels that was tokenized
		for(; it != end; it++)
		{
			// We insert the list of labels of the DNS domain name in reverse order
			dnsNamesVector.push_front(it->str());
		}

		// We rewrite the DNS domain name by iterating the reversed label list
		// and we append a dot character per each label
		for (string dnsName: dnsNamesVector)
		{
			reverseDNSName += "." + dnsName;
		}
	}

	return reverseDNSName;
}

// Utility method used to encode dot and non ASCII characters
// It converts non ASCII characters to their corresponding decimal value
// i.e. '0x88' -> '\d136'
// It converts '.' character to its corresponding decimal value
// i.e '.' -> \d046
// In case the character is ASCII then it returns the same value
string DnsWireFormatter::getEncodedChar(const unsigned char &value)
{
	string result = "";

	if (value == '.')
	{
		result = "\\d" + std::to_string(value);
	}
	// Checking for the allowed chars in the labels (Section 2.3.1 on RFC1035)
	else if (
			((value >= 'A') && (value <= 'Z')) ||
			((value >= 'a') && (value <= 'z')) ||
			((value >= '0') && (value <= '9')) ||
			(value == '-')
			)
	{
		result = value;
	}
	else
	{
		result = "\\d" + std::to_string(value);
	}
	return result;
}
