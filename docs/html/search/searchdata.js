var indexSectionsWithContent =
{
  0: "cdgimorstu~",
  1: "dt",
  2: "dst",
  3: "cdgmst~",
  4: "mr",
  5: "r",
  6: "iou"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator"
};

