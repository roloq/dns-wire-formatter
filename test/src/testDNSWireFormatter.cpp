#include "testDNSWireFormatter.h"

const unsigned char ROOT_OCTET_VALUE = '\x00';

// You can do set-up work for each test here.
TestDNSWireFormatter::TestDNSWireFormatter()
{

}

// You can do clean-up work that doesn't throw exceptions here.
TestDNSWireFormatter::~TestDNSWireFormatter()
{

}

void TestDNSWireFormatter::SetUp()
{

}

void TestDNSWireFormatter::TearDown()
{

}

/**
 * Unit test: convertWireToPresentationDataWhenItIsNull.
 * This test case tests the method called convertWireToPresentationData()
 * when the parameter wireData is NULL.\n
 * The expected behavior is that this method returns RESULT::INVALID_ARGUMENT value
 * and the presentationData parameter is returned with an empty text
 */
TEST_F(TestDNSWireFormatter, convertWireToPresentationDataWhenItIsNull)
{
	RESULT expectedResult = RESULT::INVALID_ARGUMENT;
	RESULT result = RESULT::UNKNOWN;
	char *input = NULL;

	string output;
	string expectedOutput = "";

	result = m_dnsWireFormatter.convertWireToPresentationData(input, 0, output);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());

}

/**
 * Unit test: convertWireToPresentationDataWhenItIsEmpty.
 * This test case tests the method called convertWireToPresentationData()
 * when the parameter wireData is empty.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the presentationData parameter is returned with an empty text
 */
TEST_F(TestDNSWireFormatter, convertWireToPresentationDataWhenItIsEmpty)
{
	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	char input[1] = "";

	string output;
	string expectedOutput = "";

	result = m_dnsWireFormatter.convertWireToPresentationData(input, 0, output);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());

}

/**
 * Unit test: convertWireToPresentationDataWhenItIsRoot.
 * This test case tests the method called convertWireToPresentationData()
 * when the parameter wireData value has only a trailing zero that represents the root label.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the presentationData parameter contains a trailing dot for the root label
 */
TEST_F(TestDNSWireFormatter, convertWireToPresentationDataWhenItIsRoot)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	char input[1] = {ROOT_OCTET_VALUE};

	string output;
	string expectedOutput = ".";

	result = m_dnsWireFormatter.convertWireToPresentationData(input, 1, output);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());

}

/**
 * Unit test: convertWireToPresentationDataWhenItHasOneItem.
 * This test case tests the method called convertWireToPresentationData()
 * when the parameter wireData contains one DNS domain name.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the presentationData parameter starts with a dot followed by
 * the DNS domain name and a trailing dot for the root label
 */
TEST_F(TestDNSWireFormatter, convertWireToPresentationDataWhenItHasOneItem)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	char input[10] = "\x03\x63\x6e\x6e\x03\x63\x6f\x6d\x00";

	string output;
	string expectedOutput = ".cnn.com.";

	result = m_dnsWireFormatter.convertWireToPresentationData(input, 9, output);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());

}

/**
 * Unit test: convertWireToPresentationDataWhenItHasMoreThanOneItem.
 * This test case tests the method called convertWireToPresentationData()
 * when the parameter wireData contains more than one DNS domain name.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the presentationData parameter contains a list of DNS domain names. Each
 * DNS domain name must be separated by a line feed and they must be sorted in
 * canonical order as specified in Section 6.1 of RFC4034
 */
TEST_F(TestDNSWireFormatter, convertWireToPresentationDataWhenItHasMoreThanOneItem)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	char input[19] = "\x03\x63\x6e\x6e\x03\x63\x6f\x6d\x00\x03\x62\x62\x63\x03\x63\x6f\x6d\x00";

	string output;
	string expectedOutput = ".bbc.com.\n.cnn.com.";

	result = m_dnsWireFormatter.convertWireToPresentationData(input, 18, output);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());

}

/**
 * Unit test: convertToPresentationDataUtilWhenItIsEmpty.
 * This test case tests the method called convertToPresentationDataUtil()
 * when the parameter dnsNamesVector is an empty list of DNS domain names.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the presentationData parameter is returned with an empty text
 */
TEST_F(TestDNSWireFormatter, convertToPresentationDataUtilWhenItIsEmpty)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	vector<string> input;

	string output;
	string expectedOutput = "";

	result = m_dnsWireFormatter.convertToPresentationDataUtil(input, output);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());

}

/**
 * Unit test: convertToPresentationDataUtilWhenItIsRoot.
 * This test case tests the method called convertToPresentationDataUtil()
 * when the parameter dnsNamesVector list contains one item with a root label ".".\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the presentationData parameter is returned with a trailing dot for the rool label
 */
TEST_F(TestDNSWireFormatter, convertToPresentationDataUtilWhenItIsRoot)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	vector<string> input;
	input.push_back(".");

	string output;
	string expectedOutput = ".";

	result = m_dnsWireFormatter.convertToPresentationDataUtil(input, output);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());

}

/**
 * Unit test: convertToPresentationDataUtilWhenItHasOneItem.
 * This test case tests the method called convertToPresentationDataUtil()
 * when the parameter dnsNamesVector contains a list with one item for the DNS domain name.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the presentationData parameter starts with a dot followed by
 * the DNS domain name and a trailing dot for the root label
 */
TEST_F(TestDNSWireFormatter, convertToPresentationDataUtilWhenItHasOneItem)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	vector<string> input;
	input.push_back(".cnn.com.");

	string output;
	string expectedOutput;
	expectedOutput = ".cnn.com.";

	result = m_dnsWireFormatter.convertToPresentationDataUtil(input, output);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());

}

/**
 * Unit test: convertToPresentationDataUtilWhenItHasMoreThanOneItem.
 * This test case tests the method called convertToPresentationDataUtil()
 * when the parameter dnsNamesVector contains a list with more than one DNS domain name.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the presentationData parameter contains a list of DNS domain names. Each
 * DNS domain name must be separated by a line feed.
 */
TEST_F(TestDNSWireFormatter, convertToPresentationDataUtilWhenItHasMoreThanOneItem)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	vector<string> input;
	input.push_back(".bbc.com.");
	input.push_back(".cnn.com.");

	string output;
	string expectedOutput;
	expectedOutput = ".bbc.com.\n.cnn.com.";

	result = m_dnsWireFormatter.convertToPresentationDataUtil(input, output);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());

}

/**
 * Unit test: getFirstDNSNameFromWireDataWhenItIsNull.
 * This test case tests the method called getFirstDNSNameFromWireData()
 * when the parameter wireData is NULL.\n
 * The expected behavior is that this method returns RESULT::INVALID_ARGUMENT value
 * and the dnsName parameter is returned with an empty text
 */
TEST_F(TestDNSWireFormatter, getFirstDNSNameFromWireDataWhenItIsNull)
{

	RESULT expectedResult = RESULT::INVALID_ARGUMENT;
	RESULT result = RESULT::UNKNOWN;
	char *input = NULL;
	unsigned int fromPositionIndex = 0;
	unsigned int numOctetsRead = 0;

	string output;
	string expectedOutput = "";

	result = m_dnsWireFormatter.getFirstDNSNameFromWireData(input, 0, fromPositionIndex, output, numOctetsRead);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());

}

/**
 * Unit test: getFirstDNSNameFromWireDataWhenItIsEmpty.
 * This test case tests the method called getFirstDNSNameFromWireData()
 * when the parameter wireData is empty.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the dnsName parameter is returned with an empty text
 */
TEST_F(TestDNSWireFormatter, getFirstDNSNameFromWireDataWhenItIsEmpty)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	char input[1] = "";
	unsigned int fromPositionIndex = 0;
	unsigned int numOctetsRead = 0;

	string output;
	string expectedOutput = "";

	result = m_dnsWireFormatter.getFirstDNSNameFromWireData(input, 0, fromPositionIndex, output, numOctetsRead);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());

}

/**
 * Unit test: getFirstDNSNameFromWireDataWhenItIsRoot.
 * This test case tests the method called getFirstDNSNameFromWireData()
 * when the parameter wireData is a trailing zero that represents a root label.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the dnsName parameter is returned with a trailing dot for the rool label
 */
TEST_F(TestDNSWireFormatter, getFirstDNSNameFromWireDataWhenItIsRoot)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	char input[1] = {ROOT_OCTET_VALUE};

	unsigned int fromPositionIndex = 0;
	unsigned int numOctetsRead = 0;

	string output;
	string expectedOutput = ".";

	result = m_dnsWireFormatter.getFirstDNSNameFromWireData(input, 1, fromPositionIndex, output, numOctetsRead);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());
}

/**
 * Unit test: getFirstDNSNameFromWireDataWhenItHasOneItem.
 * This test case tests the method called getFirstDNSNameFromWireData()
 * when the parameter wireData is a DNS name in wire format.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the dnsName parameter starts with a dot followed by
 * the DNS domain name and a trailing dot for the root label
 */
TEST_F(TestDNSWireFormatter, getFirstDNSNameFromWireDataWhenItHasOneItem)
{
	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	char input[10] = "\x03\x63\x6e\x6e\x03\x63\x6f\x6d\x00";
	unsigned int fromPositionIndex = 0;
	unsigned int numOctetsRead = 0;

	string output;
	string expectedOutput = ".cnn.com.";

	result = m_dnsWireFormatter.getFirstDNSNameFromWireData(input, 9, fromPositionIndex, output, numOctetsRead);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());
}

/**
 * Unit test: getFirstDNSNameFromWireDataWhenItHasMoreThan255Octets.
 * This test case tests the method called getFirstDNSNameFromWireData()
 * when the parameter wireData contains more than 255 octets.\n
 * The expected behavior is that this method returns RESULT::INVALID_NUMBER_OCTETS value
 * according to the sections 2.3.1 through 3.1 on RFC1035 and section 11 on RFC2181.\n
 * The parameter dnsName must be returned with an empty value
 */
TEST_F(TestDNSWireFormatter, getFirstDNSNameFromWireDataWhenItHasMoreThan255Octets)
{
	RESULT expectedResult = RESULT::INVALID_NUMBER_OCTETS;
	RESULT result = RESULT::UNKNOWN;
	char input[263] = "\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x04\x65\x73\x70\x6e\x03\x63\x6f\x6d"
			"\x00";

	unsigned int fromPositionIndex = 0;
	unsigned int numOctetsRead = 0;

	string output;
	string expectedOutput = "";

	result = m_dnsWireFormatter.getFirstDNSNameFromWireData(input, 262, fromPositionIndex, output, numOctetsRead);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());
}

/**
 * Unit test: getFirstDNSNameFromWireDataWhenItHasMoreThan63Labels.
 * This test case tests the method called getFirstDNSNameFromWireData()
 * when the parameter wireData contains more than 63 labels.\n
 * The expected behavior is that this method returns RESULT::INVALID_NUMBER_LABELS value
 * according to the sections 2.3.1 through 3.1 on RFC1035 and section 11 on RFC2181.\n
 * The parameter dnsName must be returned with an empty value
 */
TEST_F(TestDNSWireFormatter, getFirstDNSNameFromWireDataWhenItHasMoreThan63Labels)
{
	RESULT expectedResult = RESULT::INVALID_NUMBER_LABELS;
	RESULT result = RESULT::UNKNOWN;
	char input[258] = "\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x03\x63\x6e\x6e\x03\x63\x6f\x6d"
			"\x00";

	unsigned int fromPositionIndex = 0;
	unsigned int numOctetsRead = 0;

	string output;
	string expectedOutput = "";

	result = m_dnsWireFormatter.getFirstDNSNameFromWireData(input, 257, fromPositionIndex, output, numOctetsRead);

	EXPECT_TRUE(expectedResult == result);

	EXPECT_STREQ(output.c_str(), expectedOutput.c_str());
}

/**
 * Unit test: getDNSVectorFromWireDataWhenItIsNull.
 * This test case tests the method called getDNSVectorFromWireData()
 * when the parameter wireData is NULL.\n
 * The expected behavior is that this method returns RESULT::INVALID_ARGUMENT value
 * and the dnsNames parameter is returned with an empty list of DNS domain names
 */
TEST_F(TestDNSWireFormatter, getDNSVectorFromWireDataWhenItIsNull)
{

	RESULT expectedResult = RESULT::INVALID_ARGUMENT;
	RESULT result = RESULT::UNKNOWN;
	char *input = NULL;

	vector<string> output;
	vector<string> expectedOutput;

	result = m_dnsWireFormatter.getDNSVectorFromWireData(input, 0, output);

	EXPECT_TRUE(expectedResult == result);

	// The vector must be empty
	EXPECT_TRUE(output.empty());

}

/**
 * Unit test: getDNSVectorFromWireDataWhenItIsEmpty.
 * This test case tests the method called getDNSVectorFromWireData()
 * when the parameter wireData is empty.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the dnsNames parameter is returned with an empty list of DNS domain names
 */
TEST_F(TestDNSWireFormatter, getDNSVectorFromWireDataWhenItIsEmpty)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	char input[1] = "";

	vector<string> output;
	vector<string> expectedOutput;

	result = m_dnsWireFormatter.getDNSVectorFromWireData(input, 0, output);

	EXPECT_TRUE(expectedResult == result);

	// The vector must be empty
	EXPECT_TRUE(output.empty());

}

/**
 * Unit test: getDNSVectorFromWireDataWhenItIsRoot.
 * This test case tests the method called getDNSVectorFromWireData()
 * when the parameter wireData contains a root label in wire format.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the dnsNames parameter is returned with one item that contains
 * a trailing dot for the rool label
 */
TEST_F(TestDNSWireFormatter, getDNSVectorFromWireDataWhenItIsRoot)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	char input[1] = {ROOT_OCTET_VALUE};

	vector<string> output;
	vector<string> expectedOutput;
	expectedOutput.push_back(".");

	result = m_dnsWireFormatter.getDNSVectorFromWireData(input, 1, output);

	EXPECT_TRUE(expectedResult == result);

	auto itOutput = output.begin();
	auto itExpectedOutput = expectedOutput.begin();

	EXPECT_EQ(output.size(), expectedOutput.size());

	while ((itOutput != output.end()) && (itExpectedOutput != expectedOutput.end()))
	{
		EXPECT_STREQ(itOutput->c_str(), itExpectedOutput->c_str());
		itOutput++;
		itExpectedOutput++;
	}
}

/**
* Unit test: getDNSVectorFromWireDataWhenItHasOneDNS.
* This test case tests the method called getDNSVectorFromWireData()
* when the parameter wireData is a DNS name in wire format.\n
* The expected behavior is that this method returns RESULT::OK value
* and the dnsNames parameter has one item that contains a dot followed by
* the DNS domain name and a trailing dot for the root label
*/
TEST_F(TestDNSWireFormatter, getDNSVectorFromWireDataWhenItHasOneDNS)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	char input[10] = "\x03\x63\x6e\x6e\x03\x63\x6f\x6d\x00";

	vector<string> output;
	vector<string> expectedOutput;
	expectedOutput.push_back(".cnn.com.");

	result = m_dnsWireFormatter.getDNSVectorFromWireData(input, 9, output);

	EXPECT_TRUE(expectedResult == result);

	auto itOutput = output.begin();
	auto itExpectedOutput = expectedOutput.begin();

	EXPECT_EQ(output.size(), expectedOutput.size());

	while ((itOutput != output.end()) && (itExpectedOutput != expectedOutput.end()))
	{
		EXPECT_STREQ(itOutput->c_str(), itExpectedOutput->c_str());
		itOutput++;
		itExpectedOutput++;
	}

}

/**
 * Unit test: getDNSVectorFromWireDataWhenItHasMoreThanOneDNS.
 * This test case tests the method called convertToPresentationDataUtil()
 * when the parameter wireData contains a list with more than one DNS domain name in wire format.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the dnsNames parameter contains a list of DNS domain names. Each
 * DNS domain name must be separated by a line feed.
 */
TEST_F(TestDNSWireFormatter, getDNSVectorFromWireDataWhenItHasMoreThanOneDNS)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	char input[19] = "\x03\x63\x6e\x6e\x03\x63\x6f\x6d\x00\x03\x62\x62\x63\x03\x63\x6f\x6d\x00";

	vector<string> output;
	vector<string> expectedOutput;
	expectedOutput.push_back(".cnn.com.");
	expectedOutput.push_back(".bbc.com.");

	result = m_dnsWireFormatter.getDNSVectorFromWireData(input, 18, output);

	EXPECT_TRUE(expectedResult == result);

	auto itOutput = output.begin();
	auto itExpectedOutput = expectedOutput.begin();

	EXPECT_EQ(output.size(), expectedOutput.size());

	while ((itOutput != output.end()) && (itExpectedOutput != expectedOutput.end()))
	{
		EXPECT_STREQ(itOutput->c_str(), itExpectedOutput->c_str());
		itOutput++;
		itExpectedOutput++;
	}

}

/**
 * Unit test: sortDNSNamesWhenVectorIsEmpty.
 * This test case tests the method called sortDNSNames()
 * when the parameter dnsNamesVector contains an empty list.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the sortedDNSNames parameter contains an empty list of DNS domain names.
 */
TEST_F(TestDNSWireFormatter, sortDNSNamesWhenVectorIsEmpty)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	vector<string> input;

	vector<string> output;
	vector<string> expectedOutput;

	result = m_dnsWireFormatter.sortDNSNames(input, output);
	EXPECT_TRUE(expectedResult == result);

	EXPECT_TRUE(output.empty());
}

/**
 * Unit test: sortDNSNamesWhenVectorIsRoot.
 * This test case tests the method called sortDNSNames()
 * when the parameter dnsNamesVector contains one item with a trailing dot for the root label.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the sortedDNSNames parameter contains one item with a trailing dot for the root label.
 */
TEST_F(TestDNSWireFormatter, sortDNSNamesWhenVectorIsRoot)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	vector<string> input;
	input.push_back(".");

	vector<string> output;
	vector<string> expectedOutput;
	expectedOutput.push_back(".");

	result = m_dnsWireFormatter.sortDNSNames(input, output);
	EXPECT_TRUE(expectedResult == result);

	auto itOutput = output.begin();
	auto itExpectedOutput = expectedOutput.begin();

	EXPECT_EQ(output.size(), expectedOutput.size());

	while ((itOutput != output.end()) && (itExpectedOutput != expectedOutput.end()))
	{
		EXPECT_STREQ(itOutput->c_str(), itExpectedOutput->c_str());
		itOutput++;
		itExpectedOutput++;
	}
}

/**
 * Unit test: sortDNSNamesWhenVectorHasOneItem.
 * This test case tests the method called sortDNSNames()
 * when the parameter dnsNamesVector contains one item with a DNS domain name in a presentation format.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the sortedDNSNames parameter contains one item with a DNS domain name in a presentation format.
 */
TEST_F(TestDNSWireFormatter, sortDNSNamesWhenVectorHasOneItem)
{

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	vector<string> input;

	vector<string> output;
	vector<string> expectedOutput;

	input.push_back(".zABC.a.EXAMPLE.");
	expectedOutput.push_back(".zabc.a.example.");

	result = m_dnsWireFormatter.sortDNSNames(input, output);
	EXPECT_TRUE(expectedResult == result);

	auto itOutput = output.begin();
	auto itExpectedOutput = expectedOutput.begin();

	EXPECT_EQ(output.size(), expectedOutput.size());

	while ((itOutput != output.end()) && (itExpectedOutput != expectedOutput.end()))
	{
		EXPECT_STREQ(itOutput->c_str(), itExpectedOutput->c_str());
		itOutput++;
		itExpectedOutput++;
	}
}

/**
 * Unit test: sortDNSNamesWhenVectorHasMoreThanOneItem.
 * This test case tests the method called sortDNSNames()
 * when the parameter dnsNamesVector contains an unsorted list of DNS domain names
 * in a presentation format.\n
 * The expected behavior is that this method returns RESULT::OK value
 * and the sortedDNSNames parameter contains a list of DNS domain names
 * sorted in canonical order according to the Section 6.1 of RFC4034.
 */
TEST_F(TestDNSWireFormatter, sortDNSNamesWhenVectorHasMoreThanOneItem) {

	RESULT expectedResult = RESULT::OK;
	RESULT result = RESULT::UNKNOWN;
	vector<string> input;

	vector<string> output;
	vector<string> expectedOutput;

	// Filling input vector
	input.push_back(".a.example.");
	input.push_back(".\\d192.z.example.");
	input.push_back(".yljkjljk.a.example.");
	input.push_back(".zABC.a.EXAMPLE.");
	input.push_back(".example.");
	input.push_back(".z.example.");
	input.push_back(".Z.a.example.");
	input.push_back(".*.z.example.");
	input.push_back(".\\d046.z.example.");
	input.push_back(".\\d128.z.example.");

	// Filling expected vector
	expectedOutput.push_back(".example.");
	expectedOutput.push_back(".a.example.");
	expectedOutput.push_back(".yljkjljk.a.example.");
	expectedOutput.push_back(".z.a.example.");
	expectedOutput.push_back(".zabc.a.example.");
	expectedOutput.push_back(".z.example.");
	expectedOutput.push_back(".*.z.example.");
	expectedOutput.push_back(".\\d046.z.example.");
	expectedOutput.push_back(".\\d128.z.example.");
	expectedOutput.push_back(".\\d192.z.example.");

	result = m_dnsWireFormatter.sortDNSNames(input, output);
	EXPECT_TRUE(expectedResult == result);

	auto itOutput = output.begin();
	auto itExpectedOutput = expectedOutput.begin();

	EXPECT_EQ(output.size(), expectedOutput.size());

	while ((itOutput != output.end()) && (itExpectedOutput != expectedOutput.end()))
	{
		EXPECT_STREQ(itOutput->c_str(), itExpectedOutput->c_str());
		itOutput++;
		itExpectedOutput++;
	}
}

/**
 * Unit test: reverseDNSNameWhenTextIsEmpty.
 * This test case tests the method called reverseDNSName()
 * when the parameter dnsName is empty.\n
 * The expected behavior is that this method returns an empty string
 */
TEST_F(TestDNSWireFormatter, reverseDNSNameWhenTextIsEmpty) {

	string expectedResult = "";
	string result = "";
	string input = "";

	result = m_dnsWireFormatter.getReverseDNSName(input);
	EXPECT_STREQ(expectedResult.c_str(), result.c_str());
}

/**
 * Unit test: reverseDNSNameWhenTextIsRoot.
 * This test case tests the method called reverseDNSName()
 * when the parameter dnsName is a trailing dot for the root label.\n
 * The expected behavior is that this method returns a trailing dot for the root label
 */
TEST_F(TestDNSWireFormatter, reverseDNSNameWhenTextIsRoot) {

	string expectedResult = ".";
	string result = "";
	string input = ".";

	result = m_dnsWireFormatter.getReverseDNSName(input);
	EXPECT_STREQ(expectedResult.c_str(), result.c_str());
}

/**
 * Unit test: reverseDNSNameWhenTextHasMoreThanOneChar.
 * This test case tests the method called reverseDNSName()
 * when the parameter dnsName contains a DNS domain name in presentation format.\n
 * The expected behavior is that this method returns a reversed DNS domain name
 * i.e. .cnn.com. -> .com.cnn.
 */
TEST_F(TestDNSWireFormatter, reverseDNSNameWhenTextHasMoreThanOneChar) {

	string expectedResult = ".com.cnn.";
	string result = "";
	string input = ".cnn.com.";

	result = m_dnsWireFormatter.getReverseDNSName(input);
	EXPECT_STREQ(expectedResult.c_str(), result.c_str());
}

/**
 * Unit test: getEncodedCharWhenValueIsPeriod.
 * This test case tests the method called getEncodedChar()
 * when the parameter value contains a dot (.) value.\n
 * The expected behavior is that this method returns the decimal
 * value of a dot sign in a special format: \d046
 */
TEST_F(TestDNSWireFormatter, getEncodedCharWhenValueIsPeriod){
	string expectedResult = "\\d46";
	string result = "";
	unsigned char input = '.';

	result = m_dnsWireFormatter.getEncodedChar(input);
	EXPECT_STREQ(expectedResult.c_str(), result.c_str());
}

/**
 * Unit test: getEncodedCharWhenValueIsNonASCII.
 * This test case tests the method called getEncodedChar()
 * when the parameter value contains a non ASCII value.\n
 * The expected behavior is that this method returns the decimal value of
 * that non ASCII in a special format:
 * i.e. 0x88 -> \d136
 */
TEST_F(TestDNSWireFormatter,getEncodedCharWhenValueIsNonASCII){
	string expectedResult = "\\d136";
	string result = "";
	unsigned char input = 0x88;

	result = m_dnsWireFormatter.getEncodedChar(input);
	EXPECT_STREQ(expectedResult.c_str(), result.c_str());
}

/**
 * Unit test: getEncodedCharWhenValueIsASCII.
 * This test case tests the method called getEncodedChar()
 * when the parameter value contains an ASCII value.\n
 * The expected behavior is that this method returns the same ASCII value:
 * i.e. 0x63 -> 0x63
 */
TEST_F(TestDNSWireFormatter, getEncodedCharWhenValueIsASCII){
	string expectedResult = "-";
	string result = "";
	unsigned char input = '-';

	result = m_dnsWireFormatter.getEncodedChar(input);
	EXPECT_STREQ(expectedResult.c_str(), result.c_str());
}
