#ifndef TEST_DNS_WIRE_FORMATTER_H
#define TEST_DNS_WIRE_FORMATTER_H

#include "gtest/gtest.h"
#include "dnsWireFormatter.h"

// The fixture for testing class DnsWireFormatter.
class TestDNSWireFormatter : public ::testing::Test {

protected:

	/**
	 * The TestDNSWireFormatter constructor.
	 * You can do set-up work before starting to run all the test cases.
	 */
	TestDNSWireFormatter();

	/**
	 * The TestDNSWireFormatter destructor.
	 * You can do clean-up work after finishing all the test cases.
	 */
    virtual ~TestDNSWireFormatter();

    /**
	 * SetUp().
	 * You can do set-up work right before being executed each test case.
	 */
    virtual void SetUp();

    /**
	 * TearDown().
	 * You can do clean-up work right after being executed each test case.
	 */
    virtual void TearDown();

	DnsWireFormatter m_dnsWireFormatter;/**< The instance of the DnsWireFormatter
	                                         class that is going to be used for testing each method */

};

#endif//TEST_DNS_WIRE_FORMATTER_H
