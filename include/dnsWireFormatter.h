#ifndef DNSWIREFORMATTER_H
#define DNSWIREFORMATTER_H

#include <vector>
#include <string.h>

using namespace std;

/**
 * enum class RESULT.
 * These are the possible values to be returned by most of the methods of the DnsWireFormatter class.
 */
enum class RESULT{
	OK,/**< This value is returned when the method executed everything without any error */
	INVALID_ARGUMENT,/**< This value is returned when the method received an invalid value as argument */
	INVALID_NUMBER_LABELS,/**< This value is returned when there are more than 63 labels in a domain name */
	INVALID_NUMBER_OCTETS,/**< This value is returned when there are more than 255 octets in a domain name */
	INVALID_NUMBER_OCTETS_PER_LABEL,/**< This value is returned when there are more than 63 octets in a label */
	UNKNOWN/**< This value is used to initialize the RESULT variables*/
};

/**
 * class DnsWireFormatter.
 * This class is in charge of converting the wire data to a presentation data format
 */
class DnsWireFormatter
{
public:

	/**
	 * The DnsWireFormatter constructor.
	 *
	 */
	DnsWireFormatter();

	/**
	 * The DnsWireFormatter destructor.
	 *
	 */
	virtual ~DnsWireFormatter();

	/**
	* This is the most important method in this class.
	* It is in charge of converting the wire data format to a presentation data format
	* @param wireData The wire data to be converted to a presentation data format.
	* @param numberOctets The number of octets that has the wire data.
	* @param presentationData Data returned in a presentation data format.
	* @return RESULT::OK if the method was executed correctly
	*/
	RESULT convertWireToPresentationData(const char *wireData, const unsigned int &numberOctets, string &presentationData);

	// From here all the methods should be private but because I need access to them from GTest then I moved them to a public scope

	/**
	* It returns the first DNS name found in the wire data according to the fromPosition parameter
	* @param wireData The wire data to be converted to a presentation data format.
	* @param numberOctets The number of octets that has the wire data.
	* @param fromPosition The position where this method has to start to look up a DNS name in the wire data
	* @param dnsName The first DNS name found in the wire data
	* @param numOctetsRead The number of octets that the DNS name contains
	* @return RESULT::OK if the method was executed correctly
	*/
	RESULT getFirstDNSNameFromWireData(const char *wireData,
									   const unsigned int &numberOctets,
									   const unsigned int &fromPosition,
									   string &dnsName,
									   unsigned int &numOctetsRead);

	/**
	* It creates a vector of DNS names based on the wire data
	* @param wireData The wire data to be converted to a presentation data format.
	* @param numberOctets The number of octets that has the wire data.
	* @param dnsNames A vector of DNS names returned by this method according to the wireData parameter.
	* @return RESULT::OK if the method was executed correctly
	*/
	RESULT getDNSVectorFromWireData(const char *wireData,
			                        const unsigned int &numberOctets,
									vector<string> &dnsNames);

	/**
	* It sorts in a canonical order a list of DNS names according to the Section 6.1 of RFC4034
	* @param dnsNamesVector The vector of DNS names to be sorted
	* @param sortedDNSNames The vector of already sorted DNS names
	* @return RESULT::OK if the method was executed correctly
	*/
	RESULT sortDNSNames(const vector<string> &dnsNamesVector, vector<string> &sortedDNSNames);

	/**
	* Utility method used to convert a vector of DNS names to a presentation data format
	* @param dnsNamesVector The vector of DNS names to be sorted
	* @param presentationData Data converted to a presentation data format according to the vector of DNS names
	* @return RESULT::OK if the method was executed correctly
	*/
	RESULT convertToPresentationDataUtil(const vector<string> &dnsNamesVector, string &presentationData);

	/**
	* Utility method used to reverse a DNS name
	* i.e. .cnn.com. -> .com.cnn.
	* @param dnsName The DNS name to be reversed
	* @return The reversed DNS name
	*/
	string getReverseDNSName(const string &dnsName);

	/**
	* Utility method used to encode dot and non ASCII characters
	* It converts non ASCII characters to their corresponding decimal value
	* i.e. '0x88' -> '\d136'
	* It converts '.' character to its corresponding decimal value
	* i.e '.' -> \d046
	* In case the character is ASCII then it returns the same value
	* @param value The character to be converted to a decimal value
	* @return The corresponding decimal value for non ASCII characters
	*/
	string getEncodedChar(const unsigned char &value);
};

#endif
